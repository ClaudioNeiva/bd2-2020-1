﻿-- Dados disponíveis
select * 
from grupo;

select * 
from modelo;

select * 
from fabricante;

insert into fabricante (nu_cnpj, nm, nu_telefone, endereco)
values ('123','VW','123123123',('Aguiar','467','Pituba',''));

insert into modelo (nm, nu_cnpj_fabricante)
values 
('Gol','123'),
('Virtus','123'),
('Polo','123');

insert into veiculo (nu_placa, vl_ano_fabricacao, vl_km_atual, dt_aquisicao, cd_modelo, cd_grupo)
values 
('ABC1234',2010,50000,'2010-05-01',(select cd from modelo where nm='Gol') ,(select cd from grupo where nm='Básico')),
('CDE4567',2010,40000,'2010-05-10',(select cd from modelo where nm='Gol') , null),
('EDF6787',2015,55000,'2015-10-20',(select cd from modelo where nm='Polo') ,(select cd from grupo where nm='Luxo'));

select *
from veiculo;

--Listar os anos de fabricação que possuem uma média de quilometragem superior à 5000km:
select 	vl_ano_fabricacao,
	avg(vl_km_atual)
from 	veiculo
group by vl_ano_fabricacao
having	avg(vl_km_atual) > 5000;

select 	vl_ano_fabricacao
from 	veiculo
group by vl_ano_fabricacao
having	avg(vl_km_atual) > 5000


-- Listar os veículos (todos os atributos) por ano de fabricação (descendente) e por placa (ascendente):
select 	*
from 	veiculo
order by vl_ano_fabricacao desc, 
	 nu_placa asc;


-- Realizar o produto cartesiano entre veículo e grupo;
select 	*
from	veiculo,
	grupo;


-- Realizar o produto cartesiano entre veículo e grupo. 
-- Filtrar o resultado para os veículos cujo cd_grupo é igual ao cd da tabela grupo;
select 	*
from	veiculo,
	grupo
where   cd_grupo = cd;

alter table veiculo add column cd int null;

select 	*
from	veiculo,
	grupo
where   veiculo.cd_grupo = grupo.cd;

select 	*
from	veiculo v,
	grupo g
where   v.cd_grupo = g.cd
and	v.vl_km_atual < 100000;


-- Listar as placas, km atual e nome do grupo e nome do modelo de todos os veículos. Filtrar os veículos com menos de 100000 km:
select 	v.nu_placa as "placa",
	v.vl_km_atual as "km atual",
	g.nm as "grupo", 
	m.nm as "modelo"
from 	veiculo v
inner join grupo g on (g.cd = v.cd_grupo)
inner join modelo m on (m.cd = v.cd_modelo)
where   v.vl_km_atual < 100000;


-- Listar os nomes dos modelos, seus fabricantes e as placas dos veículos. 
-- Sua consulta deve retornar os modelos que não possuem nenhum veículo associado.
select 	m.nm as "modelo",
	f.nm as "fabricante",
	v.nu_placa as "placa"	
from 	modelo m 
inner join fabricante f on (f.nu_cnpj = m.nu_cnpj_fabricante)
left outer join veiculo v on (v.cd_modelo = m.cd);


-- Listar os nomes dos grupo e as placas dos veículos. 
-- Sua consulta deve retornar os grupo que não possuem nenhum veículo associado e os veículos que não pertencem à nenhum grupo.
select 	g.nm as "grupo",
	v.nu_placa as "placa"	
from 	grupo g
full outer join veiculo v on (v.cd_grupo = g.cd);


-- Listar os nomes dos grupo e as placas dos veículos. 
-- Fazendo combinação de todos os grupos com todos os veículos (produto cartesiano).
select 	g.nm as "grupo",
	v.nu_placa as "placa"	
from 	grupo g
cross join veiculo v;

select 	g.nm as "grupo",
	v.nu_placa as "placa"	
from 	grupo g,
	veiculo v;

-- A junção natural utiliza os nomes dos atributos no lugar da cláudula "on".
alter table veiculo 
	drop column cd;

select * 
from veiculo;

select 	*
from 	veiculo v
inner join grupo g on (g.cd = v.cd_grupo);

alter table veiculo
	rename cd_grupo to cd;

select * 
from veiculo;

select 	*
from 	veiculo v
inner join grupo g on (g.cd = v.cd);

select 	*
from 	veiculo v
natural join grupo g;


-- Listar todos os nomes de grupo e todos os nomes de modelo em uma única lista.
-- O resultado deve vir por ordem crescente de nome.
select * from grupo;
--Resultado:
--"Básico"
--"Luxo"
--"Utilitário"

select * from modelo;
--Resultado:
--"Gol"
--"Virtus"
--"Polo"

--Resultado esperado:
--"Básico"
--"Gol"
--"Luxo"
--"Polo"
--"Utilitário"
--"Virtus"

-- Inserir um novo grupo apenas para ilustrar o comportamento do "UNION" x "UNION ALL":
alter table grupo
	drop constraint un_grupo_nm;
insert into grupo (nm)
values ('Luxo');

-- UNION: remove tuplas duplicadas do conjunto resultado.
select nm
from grupo
union
select nm
from modelo
order by 1;

-- UNION ALL: NÃO remove tuplas duplicadas do conjunto resultado.
select nm
from grupo
union all
select nm
from modelo
order by 1;


-- Exclusão de tabelas e outras estruturas

drop table if exists veiculo cascade;
drop table if exists modelo cascade;
drop table if exists grupo cascade;
drop table if exists acessorio cascade;
drop table if exists veiculo_acessorio cascade;
drop table if exists fabricante cascade;
drop table if exists tarifa cascade;
drop type if exists t_endereco cascade;

-- Criação de tabelas com as respectivas chaves primárias, restrições de unicidade e checagem.

create table veiculo (
nu_placa		char(7)		not null,
vl_ano_fabricacao	smallint 	not null,
vl_km_atual		int		not null,
dt_aquisicao		timestamp	not null,
cd_modelo		integer		not null,
cd_grupo		integer		    null,
constraint pk_veiculo
	primary key (nu_placa),
constraint ch_veiculo_ano_fabricacao
	check (vl_ano_fabricacao > 2000));

create table modelo (
cd			serial 		not null,    --int , create sequence, set default
nm			varchar(30)	not null,
nu_cnpj_fabricante	char(14)	not null,
constraint pk_modelo
	primary key (cd),
constraint un_modelo_nm
	unique (nm));

create table grupo (
cd			serial 		not null,
nm			varchar(30)	not null,
constraint pk_grupo
	primary key (cd),
constraint un_grupo_nm
	unique (nm));

create table acessorio (
sg			char(3)		not null,
nm			varchar(30)	not null,
constraint pk_acessorio
	primary key (sg),
constraint un_acessorio_sg
	unique (sg));

create table veiculo_acessorio (
nu_placa_veiculo	char(7)		not null,
sg_acessorio		char(3)		not null,
constraint pk_veiculo_acessorio
	primary key (nu_placa_veiculo,sg_acessorio));

create type t_endereco as (
nm_logradouro		varchar(100),
nu			varchar(10),
nm_bairro		varchar(100),
ds_complemento		varchar(200));
	
create table fabricante (
nu_cnpj			char(14)	not null,
nm			varchar(40)	not null,
nu_telefone		varchar(15)	not null,
endereco		t_endereco	not null,
constraint pk_fabricante
	primary key (nu_cnpj));

create table tarifa (
cd_grupo		int		not null,
dt_inicio_vigencia	date		not null,			
vl			numeric(10,2)	not null,
constraint pk_tarifa
	primary key (cd_grupo, dt_inicio_vigencia));

-- Criação das chaves estrangeiras

alter table veiculo
	add constraint fk_veiculo_modelo
		foreign key (cd_modelo)
		references modelo(cd),
	add constraint fk_veiculo_grupo
		foreign key (cd_grupo)
		references grupo(cd)
		on delete set null;
		
alter table modelo
	add constraint fk_modelo_fabricante
		foreign key (nu_cnpj_fabricante)
		references fabricante(nu_cnpj)
		on update cascade;

alter table veiculo_acessorio 
	add constraint fk_veiculo_acessorio_veiculo
		foreign key (nu_placa_veiculo)
		references veiculo (nu_placa)
		on update cascade
		on delete cascade,
	add constraint fk_veiculo_acessorio_acessorio
		foreign key (sg_acessorio)
		references acessorio (sg)
		on update cascade;

alter table tarifa
	add constraint fk_tarifa_grupo
		foreign key (cd_grupo)
		references grupo(cd);


