package br.ucsal.bes20201.bd2.aula14;

import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");

		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Aluno aluno = new Aluno("Claudio");

		em.persist(aluno);

		em.getTransaction().commit();

		System.out.println("Aluno=" + aluno);

		em.detach(aluno);

		System.out.println("em.contains(aluno)=" + em.contains(aluno));

		aluno = em.find(Aluno.class, 1);

		System.out.println("em.contains(aluno)=" + em.contains(aluno));

		em.getTransaction().begin();

		aluno.setNome("Maria da Silva");

		em.getTransaction().commit();

		emf.close();

	}

}
