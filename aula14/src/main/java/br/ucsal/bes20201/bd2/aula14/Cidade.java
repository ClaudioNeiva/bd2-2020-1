package br.ucsal.bes20201.bd2.aula14;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tab_cidade")
public class Cidade {

	@Id
	private String sigla;

	private String nome;

	@ManyToOne
	private Estado estado;

	public Cidade() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cidade(String sigla, String nome) {
		super();
		this.sigla = sigla;
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
