package br.ucsal.bes20201.bd2.aula12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Exemplo {

	private static final String URL = "jdbc:postgresql://localhost:5432/aula12";
	private static final String USER = "postgres";
	private static final String PASSWORD = "abcd1234";

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws SQLException {

		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);

		insertComObtencaoDados(connection);

		inserir(connection);

		atualizar(connection);

		inserirResultSet(connection);

		consultarCorrentista(connection);

		connection.close();

	}

	private static void insertComObtencaoDados(Connection connection) throws SQLException {
		System.out.println("Informe o nome:");
		String nome = scanner.nextLine();
		String query = "insert into correntista (nm) values (?)";
		try (PreparedStatement stmt = connection.prepareStatement(query)) {
			stmt.setString(1, nome);
			stmt.executeUpdate();
		}
	}

	private static void inserirResultSet(Connection connection) throws SQLException {
		String query = "select * from correntista order by nm asc";
		try (Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(query);) {
			rs.moveToInsertRow();
			rs.updateString("nm", "Leila");
			rs.insertRow();
		}
	}

	private static void atualizar(Connection connection) throws SQLException {
		String query = "select id, nm from correntista order by nm asc";
		try (Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(query)) {
			rs.next();
			rs.updateString("nm", "Manuela");
			rs.updateRow();
		}
	}

	private static void inserir(Connection connection) throws SQLException {
		connection.setAutoCommit(false);
		String sqlInsert = "insert into correntista (nm) values ('Vitor')";
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(sqlInsert, Statement.RETURN_GENERATED_KEYS);
			ResultSet idRs = stmt.getGeneratedKeys();
			idRs.next();
			Integer id = idRs.getInt(1);
			System.out.println("id gerador para o correntista Vitor=" + id);
		}
		// connection.rollback();
		connection.commit();
		connection.setAutoCommit(true);
	}

	private static void consultarCorrentista(Connection connection) throws SQLException {
		String query = "select id, nm from correntista order by nm asc";
		try (Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(query);) {
			while (rs.next()) {
				String nome = rs.getString("nm");
				System.out.println("Nome=" + nome);
			}

			System.out.println("-----------");
			while (rs.previous()) {
				String nome = rs.getString("nm");
				System.out.println("Nome=" + nome);
			}
		}
	}

}
