package br.ucsal.bes20201.bd2.aula17.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tab_cidade")
public class Cidade {

	@Id
	private String sigla;

	private String nome;

	// Default para OneToOne e para ManyToOne fetch = FetchType.EAGER
	@ManyToOne
	private Estado estado;

	public Cidade() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cidade(String sigla, String nome, Estado estado) {
		super();
		this.sigla = sigla;
		this.nome = nome;
		this.estado = estado;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Cidade [sigla=" + sigla + ", nome=" + nome + ", estado=" + estado.getSigla() + "-" + estado.getNome()
				+ "]";
	}

}
