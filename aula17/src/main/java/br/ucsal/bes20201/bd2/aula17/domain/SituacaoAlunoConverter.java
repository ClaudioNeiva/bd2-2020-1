package br.ucsal.bes20201.bd2.aula17.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class SituacaoAlunoConverter implements AttributeConverter<SituacaoAlunoEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoAlunoEnum attribute) {
		return attribute == null ? null : attribute.getCodigo();
	}

	@Override
	public SituacaoAlunoEnum convertToEntityAttribute(String dbData) {
		return dbData == null ? null : SituacaoAlunoEnum.valueOfCodigo(dbData);
	}

}
