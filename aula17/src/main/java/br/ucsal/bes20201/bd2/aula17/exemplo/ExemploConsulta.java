package br.ucsal.bes20201.bd2.aula17.exemplo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.ucsal.bes20201.bd2.aula17.domain.Aluno;
import br.ucsal.bes20201.bd2.aula17.domain.AlunoExterno;
import br.ucsal.bes20201.bd2.aula17.domain.Cidade;
import br.ucsal.bes20201.bd2.aula17.domain.Endereco;
import br.ucsal.bes20201.bd2.aula17.domain.Estado;
import br.ucsal.bes20201.bd2.aula17.domain.SituacaoAlunoEnum;
import br.ucsal.bes20201.bd2.aula17.dto.ResultadoDTO;

public class ExemploConsulta {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Estado estadoSP = new Estado("SP", "São Paulo", new ArrayList<>());
		Cidade cidadeSAO = new Cidade("SAO", "São Paulo", estadoSP);
		Cidade cidadeGRU = new Cidade("GRU", "Guarulhos", estadoSP);
		estadoSP.getCidades().add(cidadeSAO);
		estadoSP.getCidades().add(cidadeGRU);
		em.persist(estadoSP);
		
		Aluno alunoClaudio = new Aluno("Claudio");
		alunoClaudio.setEndereco(new Endereco("Rua 1", "Pituaçu", "123", null));
		alunoClaudio.getEndereco().setCidade(cidadeGRU);
		alunoClaudio.setSituacao(SituacaoAlunoEnum.FORMADO);
		em.persist(alunoClaudio);

		AlunoExterno alunoPedro = new AlunoExterno("Pedro", "UFBA");
		alunoPedro.setEndereco(new Endereco("Rua 2", "Pituaçu", "456", null));
		alunoPedro.getEndereco().setCidade(cidadeGRU);
		alunoPedro.setSituacao(SituacaoAlunoEnum.ATIVO);
		em.persist(alunoPedro);

		AlunoExterno alunoJoaquim = new AlunoExterno("Joaquim", "UFBA");
		alunoJoaquim.setEndereco(new Endereco("Rua 3", "Brotas", "867", null));
		alunoJoaquim.getEndereco().setCidade(cidadeSAO);
		alunoJoaquim.setSituacao(SituacaoAlunoEnum.FORMADO);
		em.persist(alunoJoaquim);

		em.getTransaction().commit();

		System.out.println("*************************");

		String oql1 = "select a from Aluno a where a.endereco.bairro = :bairro order by a.nome asc";
		TypedQuery<Aluno> query1 = em.createQuery(oql1, Aluno.class);
		query1.setParameter("bairro", "Pituaçu");
		List<Aluno> result1 = query1.getResultList();
		System.out.println("Result1 (usando stream):");
		result1.stream().forEach(System.out::println);
		System.out.println("Result1 (usando for):");
		for (Aluno aluno : result1) {
			System.out.println(aluno);
		}

		System.out.println("*************************");

		String oql2 = "select a.situacao, count(*) from Aluno a group by a.situacao";
		TypedQuery<Object[]> query2 = em.createQuery(oql2, Object[].class);
		List<Object[]> result2 = query2.getResultList();
		System.out.println("Result2:");
		for (Object[] object : result2) {
			System.out.println("situação=" + object[0] + "; quantidade=" + object[1]);
		}

		System.out.println("*************************");

		String oql3 = "select new " + ResultadoDTO.class.getCanonicalName()
				+ "(a.situacao, count(*)) from Aluno a group by a.situacao";
		TypedQuery<ResultadoDTO> query3 = em.createQuery(oql3, ResultadoDTO.class);
		List<ResultadoDTO> result3 = query3.getResultList();
		System.out.println("Result3:");
		for (ResultadoDTO resultadoDTO : result3) {
			System.out.println(resultadoDTO);
		}

		System.out.println("*************************");

		// Query query = em.createNamedQuery("alunos por situação");
		TypedQuery<Aluno> query5 = em.createNamedQuery("alunos por situação", Aluno.class);
		query5.setParameter("situacao", SituacaoAlunoEnum.FORMADO);
		List<Aluno> alunos = query5.getResultList();
		System.out.println("Result5:");
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("*************************");

		String sql = "select a.nome, c.sigla from tab_aluno a inner join tab_cidade c on (a.cidade_sigla = c.sigla)";
		Query query6 = em.createNativeQuery(sql);
		List<Object[]> result6 = query6.getResultList();
		System.out.println("Result6:");
		for (Object[] result : result6) {
			System.out.println(result[0] + "|" + result[1]);
		}

		System.out.println("*************************");

		em.clear();

		String oql4 = "select e from Estado e inner join fetch e.cidades ";
		TypedQuery<Estado> query4 = em.createQuery(oql4, Estado.class);
		List<Estado> result4 = query4.getResultList();

		em.close();
		emf.close();

		System.out.println("Result4:");
		for (Estado estado : result4) {
			System.out.println(estado);
		}

	}

}
