package br.ucsal.bes20201.bd2.aula17.dto;

import br.ucsal.bes20201.bd2.aula17.domain.SituacaoAlunoEnum;

public class ResultadoDTO {

	private SituacaoAlunoEnum situacao;

	private Long quantidade;

	public ResultadoDTO(SituacaoAlunoEnum situacao, Long quantidade) {
		super();
		this.situacao = situacao;
		this.quantidade = quantidade;
	}

	public SituacaoAlunoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAlunoEnum situacao) {
		this.situacao = situacao;
	}

	public long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public String toString() {
		return "ResultadoDTO [situacao=" + situacao + ", quantidade=" + quantidade + "]";
	}

}
