-- Trigger - motivação:
-- Não deveríamos ter dados redundantes num banco de dados - como faço para evitar inconsistência nos dados;

drop table if exists conta_corrente cascade;
drop table if exists movimento cascade;
drop table if exists saldo cascade;

create table conta_corrente (
	nu		serial		not null,
	nu_cpf		char(11)	not null,
	nm		varchar(100)	not null,
	constraint pk_conta_corrente
		primary key (nu));

create table movimento (
	nu_seq		serial		not null,
	nu_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	ds_historico	varchar(200)	    null,
	constraint pk_movimento
		primary key (nu_seq));

create table saldo (
	nu_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	constraint pk_saldo
		primary key (nu_conta, dt));

alter table movimento
	add constraint fk_movimento_conta_corrente
		foreign key (nu_conta)
		references conta_corrente;

alter table saldo
	add constraint fk_saldo_conta_corrente
		foreign key (nu_conta)
		references conta_corrente;


--1. Criar um mecanismo de AUDITORIA para modificações (INSERT, UPDATE, DELETE) de dados na tabela de MOVIMENTO;
--2. A estrutura de dados para registro da auditoria (nova tabela? tabela histórico? adicionar campos à tabela movimento?) é livre;
--3. Um ou mais triggers devem alimentar a estrutura de dados da auditoria;
--4. A estrutura de dados deve auxiliar na resposta para a pergunta: "quem" fez "o que" e "quando"?

drop table if exists movimento_audit;

create table movimento_audit (	-- guardar sempre a versão antiga do registro
	id		serial		not null,
	nm_login	varchar(40)	not null,  -- verificar o tamanho máximo de login 
	dt_hr		timestamp	not null,
	tp_operacao	char(1)		not null,
	nu_seq		serial		not null,
	nu_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	ds_historico	varchar(200)	    null,
	nm_login_orig	varchar(40)	not null,
	dt_hr_orig	timestamp	not null,
	constraint pk_movimento_audit
		primary key (id),
	constraint ch_movimento_audit_tp_operacao
		check (tp_operacao in ('U','D')));
		
create table audit (
	id		serial		not null,
	nm_login	varchar(40)	not null,  -- verificar o tamanho máximo de login 
	dt_hr		timestamp	not null,
	nm_table	varchar(40)	not null,  -- verificar o tamanho máximo de nome de tabela
	ds_registro_old	varchar(4000)	    null,
	constraint pk_movimento_audit_v1
		primary key (id));
		
alter table movimento 
	add column nm_login	varchar(40)	not null,
	add column dt_hr	timestamp	not null;


-- Trigger que atualiza os campos de login e dt_hr na tabela de movimento, a partir de operações de INSERT e UPDATE. 

create or replace function f_trg_mov_audit_ins_upd()
returns trigger
as $$
begin
	NEW.nm_login := CURRENT_USER;
	NEW.dt_hr := CURRENT_TIMESTAMP;
	return NEW;
end;
$$ language PLPGSQL;

create trigger trg_mov_audit_ins
before insert or update
on movimento
for each row
execute procedure f_trg_mov_audit_ins_upd();



-- Trigger que insere os dados originais de movimento na tabela de movimento_audit, a partir de operações de DELETE e UPDATE.

create or replace function f_trg_mov_audit_del_upd()
returns trigger
as $$
begin	
	insert into movimento_audit (
		nm_login		,	
		dt_hr			,
		tp_operacao		,
		nu_seq			,
		nu_conta		,
		dt			,
		vl			,
		ds_historico 		,
		nm_login_orig		,
		dt_hr_orig		)
	values (
		CURRENT_USER		,
		CURRENT_TIMESTAMP	,
		substr(TG_OP,1,1)	,
		OLD.nu_seq		,
		OLD.nu_conta		,
		OLD.dt			,
		OLD.vl			,
		OLD.ds_historico 	,
		OLD.nm_login		,
		OLD.dt_hr		);
	if TG_OP = 'UPDATE' then
		return NEW;
	else 
		return OLD;
	end if;
end;
$$ language PLPGSQL;

create trigger trg_mov_audit_del_upd
before delete or update
on movimento
for each row
execute procedure f_trg_mov_audit_del_upd();


-- Operações para teste do trigger

--insert into conta_corrente (nu_cpf, nm) 
--values 
--('123','Claudio');

select 	*
from 	conta_corrente;

delete 
from movimento;

delete 
from movimento_audit;

insert into movimento (nu_conta, dt, vl, ds_historico)
values 
((select nu from conta_corrente where nu_cpf='123'), '2020-01-05',  500, 'Depósito');

insert into movimento (nu_conta, dt, vl, ds_historico)
values 
((select nu from conta_corrente where nu_cpf='123'), '2020-01-08', -200, 'Pagamento de conta - Luz');

select 	*
from 	movimento;

select 	*
from 	movimento_audit;

update movimento
set ds_historico = 'CAJU'
where nu_seq = 91;

delete 
from movimento
where nu_seq = 92;







-- Consulta de movimento e movimento_audit

select *
from movimento_audit;

create view movimento_audit_consolidado 
as
-- Qual o mais velho (min(id)) registro de auditoria relativo à um nu_seq.
select			
	nu_seq			,
	nu_conta		,
	dt			,
	vl			,
	ds_historico 		,
	'I'			,
	nm_login_orig		,	
	dt_hr_orig			
from 	movimento_audit	ma1
where	ma1.id = (    select 	min(ma2.id)
			from	movimento_audit ma2
			where	ma2.nu_seq = ma1.nu_seq)

-- Quais foram as atualizações sofridas pelos registros (excluir o primeiro registro de auditoria, pois o mesmo foi considerado "inserção")
union 
select
	nu_seq			,
	nu_conta		,
	dt			,
	vl			,
	ds_historico 		,
	tp_operacao		,
	nm_login_orig		,	
	dt_hr_orig			
from movimento_audit	ma1
where	tp_operacao = 'U'
and	ma1.id <> (    select 	min(ma2.id)
			from	movimento_audit ma2
			where	ma2.nu_seq = ma1.nu_seq)

union 

-- Qual o registro atual na base
select
	nu_seq			,
	nu_conta		,
	dt			,
	vl			,
	ds_historico 		,
	'Atual'			,
	nm_login		,	
	dt_hr			
from movimento

union 

-- As exclusões realizadas (trazendo a data e hora da exclusão)
select
	nu_seq			,
	nu_conta		,
	dt			,
	vl			,
	ds_historico 		,
	tp_operacao		,
	nm_login		,	
	dt_hr			
from movimento_audit	
where	tp_operacao = 'D'

union 

-- A última auditoria de exclusão, que carregou a última atualização

select
	nu_seq			,
	nu_conta		,
	dt			,
	vl			,
	ds_historico 		,
	'U'			,
	nm_login_orig		,	
	dt_hr_orig			
from movimento_audit	ma1
where	tp_operacao = 'D'
and	ma1.id <> (    select 	min(ma2.id)
			from	movimento_audit ma2
			where	ma2.nu_seq = ma1.nu_seq)
order by 1 asc,
	 8 asc,
	 6 desc;



-- Usar a view
select	*
from	movimento_audit_consolidado
where 	nu_seq = 93;


-- A consulta da view ficou complexa devido à estratégia de não duplicar o atual dentro da tabela de auditoria. 
-- A presença do registro atual apenas no movimento reduziu o espaço utilizado no banco de dados, mas trouxe uma complexidade extra 
-- para o consumo da auditoria.

