package br.ucsal.bes20201.bd2.aula15;

public enum SituacaoAlunoEnum {

	ATIVO("atv"), TRANCADO("trc"), FORMADO("frm");

	private String codigo;

	private SituacaoAlunoEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static SituacaoAlunoEnum valueOfCodigo(String codigo) {
		for (SituacaoAlunoEnum situacao : values()) {
			if (situacao.codigo.equalsIgnoreCase(codigo)) {
				return situacao;
			}
		}
		throw new IllegalArgumentException("Codigo " + codigo + " não encontrado em SituacaoAlunoEnum.");
	}

}
