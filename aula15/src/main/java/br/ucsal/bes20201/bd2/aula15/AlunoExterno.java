package br.ucsal.bes20201.bd2.aula15;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_aluno_externo")
//@DiscriminatorValue(value = "AE")
public class AlunoExterno extends Aluno {

	private String origem;

	public AlunoExterno(String nome, String origem) {
		super(nome);
		this.origem = origem;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	@Override
	public String toString() {
		return "AlunoExterno [origem=" + origem + "]";
	}

}
