package br.ucsal.bes20201.bd2.aula15;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "tab_aluno")
@SequenceGenerator(name = "sq_aluno", sequenceName = "seq_aluno")
//@DiscriminatorColumn(name = "tipoAluno", columnDefinition = "char(2)")
//@DiscriminatorValue(value = "AL")
// @Inheritance(strategy = InheritanceType.SINGLE_TABLE) //DEFAULT
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Inheritance(strategy = InheritanceType.JOINED)
public class Aluno {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_aluno")
	private Integer matricula;

	@Column(unique = true)
	private String nome;

	private String rg;

	private String orgaoExpedidorRg;

	private String ufRg;

	@Embedded
	private Endereco endereco;

	@ElementCollection
	private List<String> telefones;

	// @Convert(converter = SituacaoAlunoConverter.class)
	@Column(columnDefinition = "situacao_aluno_t")
	private SituacaoAlunoEnum situacao;

	@ManyToMany
	@JoinTable(name = "tab_aluno_disciplina")
	private List<Disciplina> disciplinas = new ArrayList<>();

	@Transient
	private Boolean selected;

	public Aluno() {
	}

	public Aluno(String nome) {
		super();
		this.nome = nome;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoExpedidorRg() {
		return orgaoExpedidorRg;
	}

	public void setOrgaoExpedidorRg(String orgaoExpedidorRg) {
		this.orgaoExpedidorRg = orgaoExpedidorRg;
	}

	public String getUfRg() {
		return ufRg;
	}

	public void setUfRg(String ufRg) {
		this.ufRg = ufRg;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public SituacaoAlunoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAlunoEnum situacao) {
		this.situacao = situacao;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", rg=" + rg + ", orgaoExpedidorRg="
				+ orgaoExpedidorRg + ", ufRg=" + ufRg + ", endereco=" + endereco + ", telefones=" + telefones
				+ ", situacao=" + situacao + ", disciplinas=" + disciplinas + ", selected=" + selected + "]";
	}

}
