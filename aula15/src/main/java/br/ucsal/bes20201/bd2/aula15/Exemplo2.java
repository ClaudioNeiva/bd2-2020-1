package br.ucsal.bes20201.bd2.aula15;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo2 {

	// private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");

		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();
		Aluno alunoAntonio = new Aluno("Antonio");
		alunoAntonio.setSituacao(SituacaoAlunoEnum.FORMADO);
		em.persist(alunoAntonio);

		AlunoExterno alunoClaudio = new AlunoExterno("Claudio", "UFBA");
		alunoClaudio.setSituacao(SituacaoAlunoEnum.FORMADO);
		em.persist(alunoClaudio);
		em.getTransaction().commit();

		em.clear();

		// System.out.println("Tecle ENTER para continuar...");
		// scanner.nextLine();

		Aluno aluno1 = em.find(Aluno.class, 1);
		System.out.println("aluno1.situacao=" + aluno1.getSituacao());

		em.close();
		emf.close();

	}

}
